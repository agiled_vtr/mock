package cl.agiled.vtr.smartwifi.services;

//import cl.karibu.commons.web.exceptions.ServiceWebClientException;
//import com.vtr.servicesvtr.dto.comunes.ResultadoEjecucion;
//import com.vtr.servicesvtr.dto.comunes.usuarios.Usuario;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.agiled.vtr.smartwifi.model.Usuario;

/**
 *
 * @author dbustamante
 */

@Component
public class UsuariosService 
{
    static final String USUARIOS_SERVICE_URL = "services.usuarios.url";
    static final String USUARIOS_SERVICE_ERROR_URL = "services.usuarios.error.url";
    static final String USUARIOS_SERVICE_ERROR_INVOCATION = "services.usuarios.error.invocation";
    static final String USUARIOS_SERVICE_ERROR_RESULT = "services.usuarios.error.result";

    static final String CACHE_NAME_USUARIO_4RUT = "usuarios_por_rut_uup";
	
    private Logger log = LoggerFactory.getLogger(UsuariosService.class);

    public Usuario obtenerUsuario(String rut)
    {
	//log.info("********* UsuariosService obtenerUsuario()");
	Usuario usuario = new Usuario();
	usuario.setRut(rut);
	usuario.setCalle("Apoquindo 4800");
	usuario.setCasaDeptoParcela("");
	usuario.setCelularDePrepago("56999999999");
	return usuario;
    }


/*
    @org.springframework.cache.annotation.Cacheable(value = CACHE_NAME_USUARIO_4RUT, unless = "#result == null")
    public Usuario obtenerUsuario(String rut);

    @org.springframework.cache.annotation.CacheEvict(value = CACHE_NAME_USUARIO_4RUT)
    public void deleteChacheObtenerUsuario(String rut);

    public String obtenerCorreoClienteNotificacion(Usuario usuario);

*/

//    @org.springframework.cache.annotation.CacheEvict(value = CACHE_NAME_USUARIO_4RUT)
//    ResultadoEjecucion cambiarIdUsuarioSinNotificar(String usuarioActual, String nuevoUsuario) throws ServiceWebClientException;
}
