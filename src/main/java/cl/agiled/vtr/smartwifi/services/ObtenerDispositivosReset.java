package cl.agiled.vtr.smartwifi.services;

//import org.springframework.context.annotation.PropertySource;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;

import java.io.IOException;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.StringEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.DeserializationFeature;

import cl.karibu.commons.web.dto.DTO;
import com.vtr.servicesvtr.dto.reset.OutDispositvosReset;

//@Configuration
//@PropertySource("file:cl/vtr/ms-vtr-vod-service/application.properties")
public class ObtenerDispositivosReset
{
    private static Logger log = LoggerFactory.getLogger(ObtenerDispositivosReset.class);

    public static DTO<List<OutDispositvosReset>> doObtenerDispositivosReset(String endpoint, String idCuenta, String rutCliente) throws IOException 
    {
	String UUP_RUT = StringUtils.leftPad(rutCliente, 12, "0");
	String UUP_UID = StringUtils.leftPad(rutCliente, 12, "0");

	CloseableHttpClient client = HttpClients.createDefault();
	HttpPost request   = new HttpPost(endpoint);
	request.setHeader("UUP_RUT", UUP_RUT);
	request.setHeader("UUP_UID", UUP_UID);
	request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

	String json = "{\"idCuenta\":\"" + idCuenta + "\",\"rutCliente\":\"" + rutCliente + "\"}";
    	StringEntity stringEntity = new StringEntity(json);
    	request.setEntity(stringEntity);

	log.info("REQUEST: " + endpoint);
	log.info("" + json);

	try
	{
  	    CloseableHttpResponse response = client.execute(request);

	    if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
	    {
		String result = "";
		HttpEntity entity = response.getEntity();
                if (entity != null) 
		{
                    // return it as a String
                    result = EntityUtils.toString(entity);
                    log.debug(result);
                }

		ObjectMapper objectMapper = new ObjectMapper();

		TypeReference ref = new TypeReference<DTO<List<OutDispositvosReset>>>() {};

		DTO<List<OutDispositvosReset>> dto = new DTO<List<OutDispositvosReset>>();
		//dto = objectMapper.readValue(result, DTO.class);
		dto = objectMapper.readValue(result, ref);

		log.debug("status:" + dto.getStatus());
	    	//response.close();
		//log.info("Status :" + authUserResponse.getStatus());
		//log.info("Message:" + authUserResponse.getMessage());

	    	response.close();
		client.close();
		return dto;
	
	    }
	    else
	    {
		log.debug("Bad status:" + response.getStatusLine().getStatusCode());
	    }

	    response.close();
	}
	catch (Exception e)
	{
	    log.error("Error",  e);
	}

	client.close();
	return null;

    }

}

