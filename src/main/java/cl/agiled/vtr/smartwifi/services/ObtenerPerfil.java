package cl.agiled.vtr.smartwifi.services;

//import org.springframework.context.annotation.PropertySource;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.DeserializationFeature;

import cl.karibu.commons.web.dto.DTO;
import com.vtr.servicesvtr.dto.boleta.PerfilCliente;

//@Configuration
//@PropertySource("file:cl/vtr/ms-vtr-vod-service/application.properties")
public class ObtenerPerfil
{
    private static Logger log = LoggerFactory.getLogger(ObtenerPerfil.class);

    private  static String obtenerPerfilURL = "http://172.17.209.161:7003/boleta/obtenerPerfil.json";

    //@Value("${getVtrVodURL.endpoint}")
    //private String getVtrVodURL;

    public void initParams() 
    {
	log.info("Init configuration parameters");
    }
	

    public static DTO<PerfilCliente> doObtenerPerfil(String endpoint, String rutCliente) throws IOException 
    {
	String UUP_RUT = StringUtils.leftPad(rutCliente, 12, "0");
	String UUP_UID = StringUtils.leftPad(rutCliente, 12, "0");

	CloseableHttpClient client = HttpClients.createDefault();
	HttpGet request   = new HttpGet(endpoint);
	request.setHeader("UUP_RUT", UUP_RUT);
	request.setHeader("UUP_UID", UUP_UID);
	request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

	log.info("REQUEST: " + endpoint);
	log.debug("[UUP_RUT:" + UUP_RUT + "][UUP_UID:" + UUP_UID + "]" );

	try
	{
  	    CloseableHttpResponse response = client.execute(request);

	    if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
	    {
		String result = "";
		HttpEntity entity = response.getEntity();
                if (entity != null) 
		{
                    // return it as a String
                    result = EntityUtils.toString(entity);
                    //log.debug(result);
                }

		ObjectMapper objectMapper = new ObjectMapper();

		TypeReference ref = new TypeReference<DTO<PerfilCliente>>() {};

		DTO<PerfilCliente> dto = new DTO<PerfilCliente>();
		dto = objectMapper.readValue(result, ref);

		log.debug("status:" + dto.getStatus());
		PerfilCliente perfilCliente = (PerfilCliente) dto.getData();

	    	//response.close();
		//log.info("Status :" + authUserResponse.getStatus());
		//log.info("Message:" + authUserResponse.getMessage());

	    	response.close();
		client.close();
		return  dto;
	
	    }
	    else
	    {
		log.debug("Bad status");
	    }

	    response.close();
	}
	catch (Exception e)
	{
	    log.error("Error",  e);
	}

	client.close();
	return null;

    }

}

