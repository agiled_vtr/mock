package cl.agiled.vtr.smartwifi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import cl.agiled.vtr.smartwifi.model.*;

@SpringBootApplication
@ComponentScan(basePackages = {"cl.agiled.vtr.smartwifi"}, lazyInit = true)
@PropertySource(value = {"file:cl/vtr/ms-smartwifi-service/application.properties"})
public class SmartWifiApplication 
{
	private Logger log = LoggerFactory.getLogger(SmartWifiApplication.class);
	
	@Autowired
    	private ApplicationContext applicationContext;

	@Value("${redis.cluster}")
	private String redisCluster;

	@Bean
	JedisConnectionFactory jedisConnectionFactory() 
	{
		log.info("CONFIGURE REDIS CONNECTION FACTORY: " + redisCluster);

		if (redisCluster != null && redisCluster.contains(",")) 
		{
			RedisClusterConfiguration cluster = new RedisClusterConfiguration();

			String node[] = redisCluster.split(",");
			for (int i = 0; i < node.length; i++) 
			{
				log.info("ADD REDIS CLUSTER NODE " + node[i]);
				String param[] = node[i].split(":");
				cluster.clusterNode(param[0], Integer.parseInt(param[1]));
			}

			return new JedisConnectionFactory(cluster);
		} 
		else 
		{
			log.info("USING DEFAULT REDIS CONFIGURATION");
			return new JedisConnectionFactory(new RedisStandaloneConfiguration("localhost", 6379));
		}
	}

	@Bean
	CacheManager cacheManager() 
	{
		log.info("CONFIGURE REDIS CACHE MANAGER");
		return RedisCacheManager.create(jedisConnectionFactory());
	}
	
	@Bean
	RedisTemplate<String, String> redisTemplate() 
	{	
		RedisTemplate<String, String> template = new RedisTemplate<String, String>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}
}
