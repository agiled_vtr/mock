package cl.agiled.vtr.smartwifi;

import java.io.File;
import java.io.InputStream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
//import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmartWifiListener implements ServletContextListener 
{
	private Logger log = LoggerFactory.getLogger(SmartWifiListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) 
	{
			
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) 
	{
	    try
	    {
		LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
        	File file = new File("cl/vtr/ms-smartwifi-service/log4j2.xml");

        	// this will force a reconfiguration
        	context.setConfigLocation(file.toURI());
	    }
	    catch (Exception e)
	    {

	    }

	    log.info("Starting MS SmartWifi Service");
	    log.info("Listening...");
	}

}
