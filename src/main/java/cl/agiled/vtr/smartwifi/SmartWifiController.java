package cl.agiled.vtr.smartwifi;

import java.time.Duration;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import org.apache.http.client.ClientProtocolException;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;

import cl.agiled.vtr.smartwifi.model.*;
import cl.agiled.vtr.smartwifi.util.Serializer;
import cl.agiled.vtr.smartwifi.util.ELKClient;
import cl.agiled.vtr.smartwifi.services.ObtenerPerfil;
import cl.agiled.vtr.smartwifi.services.ObtenerProductos;
import cl.agiled.vtr.smartwifi.services.ObtenerDispositivosReset;

import cl.karibu.commons.web.dto.DTO;
import cl.karibu.commons.web.utils.Utils;
import com.vtr.servicesvtr.dto.boleta.PerfilCliente;
import com.vtr.servicesvtr.dto.boleta.Cuenta;
import com.vtr.servicesvtr.dto.boleta.Activo;
import com.vtr.servicesvtr.dto.direccion.Direccion;
import com.vtr.servicesvtr.dto.reset.OutDispositvosReset;
import com.vtr.servicesvtr.dto.productos.DetalleProducto;
import com.vtr.servicesvtr.dto.productos.Producto;
import com.vtr.servicesvtr.dto.productosrelacionados.ProductoRelacionado;

@RestController
@PropertySource("file:cl/vtr/ms-smartwifi-service/application.properties")
public class SmartWifiController extends SpringBeanAutowiringSupport 
{
    private Logger log = LoggerFactory.getLogger(SmartWifiController.class);

    private final String UUP_RUT = "UUP_RUT";
    private final String UUP_UID = "UUP_UID";

    @Autowired
    CacheManager cacheManager;
	
    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Value("${cache.name}")
    String cacheName;

    @Value("${cache.ttl:86400}")
    int cacheTTL;

    @Value("${obtenerPerfil.endpoint}")
    String obtenerPerfilEndpoint;

    @Value("${obtenerDispositivosReset.endpoint}")
    String obtenerDispositivosResetEndpoint;

    @Value("${obtenerProductos.endpoint}")
    String obtenerProductosEndpoint;

    @Value("${fixed.response:true}")
    boolean fixedResponse;

    @RequestMapping(value = "/oidc-ux/v1/{businessid}/userinfo", produces = { "application/json", "text/plain" }, method = RequestMethod.GET)
    public ResponseEntity<?> userinfo(HttpServletRequest request, @RequestHeader HttpHeaders headers, @PathVariable("businessid") String businessid, @RequestParam(value = "accountIds", required = false) List<String> accountIds) 
    {
	log.debug("***** userinfo Init *****");
	log.debug(request.getRequestURI() + " " + request.getQueryString());

	if(fixedResponse)
	{
	    List<AccountId> accountIdsList = new ArrayList<AccountId>();

	    String json = "[ {\"accountId\":\"12345678\", \"businessId\":\"CL\", \"addressLines\":[\"Algun Lugar en San Juan\",\"San Juan\",\"Chile\"], \"dataServices\":[{\"serviceId\":\"98765\", \"serviceDescription\":\"Internet 100MB\", \"modems\":[{\"macAddress\":\"9f:39:6b:05:56:d1\"}]}]},   {\"accountId\":\"11315979\",\"businessId\":\"CL\", \"addressLines\":[\"Otro Lugar en San Juan\", \"San Juan\", \"Chile\"], \"dataServices\":[{\"serviceId\":\"35727\", \"serviceDescription\":\"Internet 200MB\", \"modems\":[{\"macAddress\":\"9f:29:6b:15:56:f4\"}]}]}]";
	 
	    try
	    {
	    	TypeReference ref = new TypeReference<List<AccountId>>() {};

	    	ObjectMapper objectMapper = new ObjectMapper();

	    	accountIdsList = objectMapper.readValue(json, ref);

		log.debug("RESPONSE(fixed):" + objectMapper.writeValueAsString(accountIdsList));

		return ResponseEntity.ok().body(accountIdsList);		
	    }
	    catch(Exception e)
	    {
		log.debug(e.getMessage());
	    	String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    	return new ResponseEntity<String>(response, HttpStatus.BAD_REQUEST);
	    }	
       
	}
    	else
    	{	

	    //log.debug("[UUP_RUT:" + headers.getFirst(UUP_RUT) + "][UUP_UID:" + headers.getFirst(UUP_UID) + "]" );

            //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            //Usuario usuario = (Usuario) auth.getPrincipal();
	    //String elkID = ELKClient.getID();
	    //log.info("Request userinfo - businessid:" + businessid + " accountIds:" + accountIds.toString());
	    //ELKClient.notifyELK(elkID,"SmartWifi-service-Request","");

	log.info("#########################################################################");

	if(!businessid.equals("CL"))
	{
	    String response = "{ \"code\": \"BUS_INVALIDBUID\", \"message\": \"Invalid businessId.\"}";
	    return new ResponseEntity<String>(response, HttpStatus.BAD_REQUEST);
	}

	if(accountIds == null || accountIds.size() < 1)
	{
	    String response = "{ \"code\": \"BUS_BADREQUEST\", \"message\": \"The provided request was wrong.\"}";
	    return new ResponseEntity<String>(response, HttpStatus.BAD_REQUEST);
	}

	// Consideramos solo el primer valor de cuentas entregadas como RUT
	String p_rutCliente = accountIds.get(0);
	
	if(!Utils.validaRut(p_rutCliente))
	{
	    String response = "{ \"code\": \"BUS_BADREQUEST\", \"message\": \"The provided request was wrong.\"}";
	    return new ResponseEntity<String>(response, HttpStatus.BAD_REQUEST);
	}

	DTO<PerfilCliente> dtoPerfilCliente = new DTO<PerfilCliente>();

	try
	{
	    // Revisar : UUP_RUT y UUP_UID son los tokens pero se debe llamar al método por el RUT que viene en el accountID
	    dtoPerfilCliente = ObtenerPerfil.doObtenerPerfil(obtenerPerfilEndpoint, p_rutCliente);
	}
	catch(Exception e)
	{
	    log.debug("Error en ObtenerPerfil:" + e.getMessage());
	    String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	if(dtoPerfilCliente == null)
	{
	    log.debug("Respuesta NULL");
	    String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	if(dtoPerfilCliente.getStatus() != 200)
	{
	    log.debug("Error en Status:" + dtoPerfilCliente.getStatus());
	    String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	List<AccountId> accountIdsList = new ArrayList<AccountId>(); // Response 

	PerfilCliente perfilCliente = (PerfilCliente) dtoPerfilCliente.getData();

	List<Cuenta> listCuenta = perfilCliente.getCuentas();
	
	if(listCuenta.size() == 0)
	{
	    log.debug("El usuario no tiene cuentas asociadas.");
	    return ResponseEntity.ok().body(accountIdsList);		
	}

	for(Cuenta cuenta : listCuenta)
	{
	    if(!cuenta.isCuentaMovil() && cuenta.isPoseeInternet())
	    {
		boolean addAccountId = true;
		// Agregamos dato para response
	    	AccountId accountId = new AccountId();
		accountId.setAccountId(cuenta.getCodigo());
	    	accountId.setBusinessId(businessid);
	    	List<String> addressLines = new ArrayList<String>();
	    	addressLines.add(cuenta.getDatosDireccion().getDireccion());
	    	addressLines.add(cuenta.getDatosDireccion().getComuna());
	    	addressLines.add(cuenta.getDatosDireccion().getCiudad());
	    	accountId.setAddressLines(addressLines);

		/*
		*/
		List<DataService> dataServices = new ArrayList<DataService>();

		List<Activo> listActivo = cuenta.getActivos();

		for(Activo activo : listActivo)
		{
		    if(activo.getxCodiClasserv() != null && activo.getxCodiClasserv().equals("2WAY"))
		    {
			addAccountId = true;
 	    		DataService dataService = new DataService();
	    		dataService.setServiceId("" + activo.getIdServicio());
			dataService.setServiceDescription(activo.getCodigo());
			dataServices.add(dataService);
		    }
		}

	    	accountId.setDataServices(dataServices);
		/*
		*/

		if(addAccountId)
		{
	    	    accountIdsList.add(accountId);
		}
	    }
	}

	// Ciclamos sobre cuentas para obtener resultado dispositivos RESET

	log.info("#########################################################################");

	for(AccountId accountId : accountIdsList)
	{
	  try
	  {
	    String p_idCuenta   = accountId.getAccountId();
	    //String p_rutCliente = headers.getFirst(UUP_RUT);
	       
	    DTO<List<OutDispositvosReset>> dto = new DTO<List<OutDispositvosReset>>();
	    try
	    {
	    	dto = ObtenerDispositivosReset.doObtenerDispositivosReset(obtenerDispositivosResetEndpoint, p_idCuenta, p_rutCliente);
	    }
	    catch(Exception e)
	    {
	    	log.debug("Error en ObtenerDispositivosReset:" + e.getMessage());
	    	String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    	return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	    if(dto == null)
	    {
	    	String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    	return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	    if(dto.getStatus() != 200)
	    {
	    	log.debug("Error en Status:" + dtoPerfilCliente.getStatus());
	    	String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    	return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	    }

	    //log.debug("DATA:" + dto.getData()); 

	    List<OutDispositvosReset> listOutDispositvosReset = (List<OutDispositvosReset>) dto.getData();

 	    //List<DataService> listDataService = new ArrayList<DataService>();

	    for(OutDispositvosReset outDispositvosReset : listOutDispositvosReset)
	    {
		if(outDispositvosReset.getTipoServicio() != null && outDispositvosReset.getTipoServicio().equals("2WAY"))
		{
		/*
		    DataService dataService = new DataService();
		    dataService.setServiceId(outDispositvosReset.getIdServicio());	

		    List<ProductoRelacionado> listProductoRelacionado = outDispositvosReset.getProductosRelacionados();
		    List<Modem> listModem = new ArrayList<Modem>();
		    for(ProductoRelacionado productoRelacionado : listProductoRelacionado)
		    {
		    	// Asociamos el Modem al Servicio de la Cuenta
		    	Modem modem = new Modem();
		    	modem.setMacAddress(productoRelacionado.getMac());
		    	listModem.add(modem);
		    }
		    dataService.setModems(listModem);
		    listDataService.add(dataService);
		*/
		    
		/*
		*/
		    List<DataService> listDataService = accountId.getDataServices();
		    for(DataService dataService : listDataService)
		    {
			if(dataService.getServiceId() != null && dataService.getServiceId().equals(outDispositvosReset.getIdServicio()))
			{
		    	    List<ProductoRelacionado> listProductoRelacionado = outDispositvosReset.getProductosRelacionados();
			    List<Modem> listModem = new ArrayList<Modem>();
		    	    for(ProductoRelacionado productoRelacionado : listProductoRelacionado)
		    	    {
			    	// Asociamos el Modem al Servicio de la Cuenta
			    	Modem modem = new Modem();
			    	modem.setMacAddress(productoRelacionado.getMac());
			    	listModem.add(modem);
		    	    }
			    dataService.setModems(listModem);
			}
		    }
		/*
		*/
		}	
	    }

	    //if(listDataServices.size() > 0)
	    {
		//accountId.setDataServices(listDataServices);
	    }
	  }
	  catch (Exception e)
	  {
	    log.error("Error",  e);
	  }
	}

	log.info("#########################################################################");

	DTO<List<Producto>> dtoListProducto = new DTO<List<Producto>>();

	try
	{
	    dtoListProducto = ObtenerProductos.doObtenerProductos(obtenerProductosEndpoint, p_rutCliente);
	}
	catch(Exception e)
	{
	    log.debug("Error en ObtenerPerfil:" + e.getMessage());
	    String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	if(dtoListProducto == null)
	{
	    log.debug("Respuesta NULL");
	    String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	if(dtoListProducto.getStatus() != 200)
	{
	    log.debug("Error en Status:" + dtoPerfilCliente.getStatus());
	    String response = "{ \"code\": \"SYS_INTERNALERROR\", \"message\": \"An internal error has occurred\"}";
	    return new ResponseEntity<String>(response, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	List<Producto> listProducto = (List<Producto>) dtoListProducto.getData();

	for(AccountId accountId : accountIdsList)
	{
	    for(Producto producto : listProducto)
	    {
		if(accountId.getAccountId().equals(producto.getCodigo()))
		{
		    List<DataService> listDataService = accountId.getDataServices();

		    for(DataService dataService : listDataService)
		    {
			List<DetalleProducto> listDetalleProducto = producto.getDetalles();
			for(DetalleProducto detalleProducto : listDetalleProducto)
			{
			    if(detalleProducto.getIdServicio().equals(dataService.getServiceId()))
			    {
				if(detalleProducto.getTipo().equals("2WAY"))
			 	{
		    	    	    dataService.setServiceDescription(detalleProducto.getNombre());
				    break;
			 	}
			    }
			}
		    }
		}
	    }
	}

	//ELKClient.notifyELK(elkID,"SmartWifi-service-Response", accountIdsList);
	try
	{
	    ObjectMapper objectMapper     = new ObjectMapper();
	    log.debug("RESPONSE:" + objectMapper.writeValueAsString(accountIdsList));
	}
	catch (Exception e)
	{
	    log.debug(e.getMessage());
	}

	return ResponseEntity.ok().body(accountIdsList);		

	}
    }

    public void putCache(String cacheName, String key, Object o, int ttl) 
    {
   	//log.info("Put cache with key " + key);
	try 
	{
	    RedisCacheWriter redisCacheWriter = (RedisCacheWriter) cacheManager.getCache(cacheName).getNativeCache();
	    String token = cacheName + "::" + key;
	    log.debug("Put Redis token ->" + token + "<- with TTL ->" + ttl + "<-");
	    redisCacheWriter.put(cacheName, token.getBytes(), Serializer.serialize(o), Duration.ofSeconds(ttl));
	} 
	catch (Exception e) 
	{
	    log.error("Error to connect with REDIS", e);
	}
    }
}
