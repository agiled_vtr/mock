package cl.agiled.vtr.smartwifi.security;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author dbustamante
 */
public class OAMAuthentication implements Authentication 
{
	private static final long serialVersionUID = 1L;

	private String rut;

	public OAMAuthentication(String rut) {
		this.rut = rut;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.emptyList();
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getDetails() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return null;
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public void setAuthenticated(boolean bln) throws IllegalArgumentException {
	}

	@Override
	public String getName() {
		return this.rut;
	}

}
