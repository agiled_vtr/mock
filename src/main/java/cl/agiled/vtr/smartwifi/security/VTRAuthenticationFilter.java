package cl.agiled.vtr.smartwifi.security;

//import static cl.karibu.commons.web.boot.AbstractRootApplication.PROFILE_DEV;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

//import cl.agiled.vtr.smartwifi.util.Utils;
import cl.agiled.vtr.smartwifi.model.VTRUser;
import cl.agiled.vtr.smartwifi.model.Usuario;
import cl.agiled.vtr.smartwifi.services.UsuariosService;

//import com.vtr.servicesvtr.dto.VTRUser;
//import com.vtr.servicesvtr.dto.comunes.usuarios.ConsultarMigradoUsuarioOut;
//import com.vtr.servicesvtr.dto.comunes.usuarios.Usuario;
//import com.vtr.servicesvtr.services.usuarios.ServiceConsultarEstadoMigradoUsuario;
//import com.vtr.servicesvtr.services.usuarios.UsuariosService;

//import cl.karibu.commons.logger.LoggerName;
//import cl.karibu.commons.web.dto.comunes.ParNombreValor;
//import cl.karibu.commons.web.services.PropertyService;
import cl.karibu.commons.web.utils.Utils;



/**
 *
 * @author dbustamante
 */
@Component
public class VTRAuthenticationFilter extends OncePerRequestFilter 
{
	private Logger log = LoggerFactory.getLogger(VTRAuthenticationFilter.class);

	//private static final Logger LOGGER_PER 	  	 = LoggerFactory.getLogger(LoggerName.PERFORMANCE);
	//private static final Logger LOGGER_SEG 		 = LoggerFactory.getLogger(LoggerName.SEGURIDAD);

	private static final String CONSULTARMIGRADB 	 = "var.global.usuariomigrado.usarmigradb";

	public static final String KEY_RUT 	     	 = "uup_rut";
	public static final String KEY_UID 	     	 = "uup_uid";
	public static final String KEY_MIGRADO 	 	 = "uup_migrado";
	public static final String KEY_CONTACTO_CRM_USER = "uup_contactocrm_usuario";

	private static final String KEY_LOG_CLIENTE 	 = "cliente";
	private static final String KEY_LOG_ID_REQUEST 	 = "requestId";

	//@Autowired
	//Environment env;
	@Autowired
	private UsuariosService usuariosService;
	//@Autowired
	//private PropertyService propertyService;
	//@Autowired
	//private ServiceConsultarEstadoMigradoUsuario usuarioMigradoService;

	@Override
	protected void doFilterInternal(HttpServletRequest request,HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException 
	{
	    log.info("VTRAuthenticationFilter.doFilterInternal - begin");
	    long inicio = System.currentTimeMillis();
	    long fin = 0;
	    String url = request.getRequestURI();

	    // long ident=(new Timestamp(new Date().getTime())).getTime();	request.setAttribute(KEY_LOG_ID_REQUEST, ""+ident);
	    // RequestContextHolder.setRequestAttributes((RequestAttributes) request.getAttribute(KEY_LOG_ID_REQUEST),true);
	    // MDC.put(KEY_LOG_ID_REQUEST,StringUtils.EMPTY+ ident);
		/**
		 * ver
		 * cl.karibu.commons.web.boot.AbstractRootApplication.configProfile()
		 */
	    //if (env.acceptsProfiles(PROFILE_DEV)) 
	    //{
	    //	modificarProperties(request, true);
	    //}

	    if (!isPublic(request)) 
	    {
	    	//log.info("*** VTRAuthenticationFilter.doFilterInternal - validate");
		VTRUser user = validate(request);

		if (user == null || user.getPrincipal() == null) 
		{
		    fin = System.currentTimeMillis();
		    //LOGGER_PER.info("Tiempo de ejecución de respuesta [{}]ms para [{}]", fin - inicio, url);
		    response.setStatus(403);
		    //MDC.remove(KEY_LOG_CLIENTE);
		    //MDC.remove(KEY_LOG_ID_REQUEST);
		    return;
		}

		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
		SecurityContextHolder.getContext().setAuthentication(user);
		filterChain.doFilter(request, response);
		//if (env.acceptsProfiles(PROFILE_DEV)) 
	 	//{
		//    modificarProperties(request, false);
		//}
	    } 
	    else 
	    {
		log.info("***** public = FALSE");
		filterChain.doFilter(request, response);
		//if (env.acceptsProfiles(PROFILE_DEV)) 
		//{
		//    modificarProperties(request, false);
		//}
	    }
	    fin = System.currentTimeMillis();
	    //LOGGER_PER.info("Tiempo de ejecución de respuesta [{}]ms para [{}]", fin - inicio, url);
	    //MDC.remove(KEY_LOG_CLIENTE);
	    //MDC.remove(KEY_LOG_ID_REQUEST);
	    log.info("VTRAuthenticationFilter.doFilterInternal - end");
	}

	private boolean isPublic(HttpServletRequest request) 
	{
	    //log.info("VTRAuthenticationFilter.isPublic - begin");
	    return false;

	    /*

	    boolean ispublic = false;
	    String url = request.getRequestURI().substring(request.getContextPath().length());
	    //LOGGER_SEG.trace("Verificando si es publico {}", url);

	    List<ParNombreValor> publicUrls = propertyService.getListProperties("security.url.public");
	    if (publicUrls != null) 
	    {
		for (ParNombreValor pnv : publicUrls) 
		{
		    //LOGGER_SEG.trace("PNV {}\t== {}", pnv.getNombre(), pnv.getValor()); 
			if (url.startsWith(pnv.getValor())) 
			{
			    ispublic = true;
			    break;
			}
		}
	    }
	    //LOGGER_SEG.debug("{} PUBLIC == {}", url, ispublic);
	    return ispublic;

	    */
	}

	private VTRUser validate(HttpServletRequest request) 
	{
	    //log.info("VTRAuthenticationFilter.validate - begin");

	    Map<String, String> headers = new HashMap<String, String>();
	    @SuppressWarnings("unchecked")
	    Enumeration<String> headerNames = request.getHeaderNames();
	    String key, value;
	    //LOGGER_SEG.trace("HEADERS...");
	    //log.debug("HEADERS:");
	    while (headerNames.hasMoreElements()) 
	    {
		key = headerNames.nextElement();
		key = (key == null ? null : key.toLowerCase());
		value = request.getHeader(key);
		headers.put(key, value);
		//log.debug("" + key + " => " + value + "");
		//LOGGER_SEG.trace("\t... {} -- {}", key, value);
	    }

	    VTRUser user = null;
	    String HEADER_UUP_RUT = headers.get(KEY_RUT);
	    String HEADER_UUP_UID = headers.get(KEY_UID);
	    String HEADER_APP_CONTACTO_CRM_USER = headers.get(KEY_CONTACTO_CRM_USER);

	    //boolean usarMigraDB = propertyService.getBoolProperty(CONSULTARMIGRADB);
	    boolean usarMigraDB= true;
	    boolean migrado    = false;
	    boolean error      = false;

	    Date fechaMigracion = Calendar.getInstance().getTime();

	    if (usarMigraDB) 
  	    {
		/*
		ConsultarMigradoUsuarioOut usuarioMigradoOut;
	 	try 
		{
		    usuarioMigradoOut = usuarioMigradoService.ConsultarEstadoMigradoUsuario(Utils.rutSinCeros(HEADER_UUP_RUT));
		    migrado 	      = usuarioMigradoOut.getEsClienteServicioMigrado();
		    fechaMigracion    = usuarioMigradoOut.getFechaMIgracion();
		    LOGGER_SEG.info("CONSULTANDO ESTADO DE USUARIO MIGRADO EN MIGRADB");
		    LOGGER_SEG.info("HEADERS... RUT: {}, UID: {}, MIG: {}", HEADER_UUP_RUT, HEADER_UUP_UID, migrado);
		} 
		catch (Exception e) 
		{
		    error =true;
		}
		*/
	    } 
	    else 
	    {
		String HEADER_FLAG_MIGRADO = request.getHeader(KEY_MIGRADO);
		migrado = Boolean.valueOf(HEADER_FLAG_MIGRADO);
		//LOGGER_SEG.info("CONSULTANDO ESTADO DE USUARIO MIGRADO POR HEADER");
		//LOGGER_SEG.info("HEADERS... RUT: {}, UID: {}, MIG: {}", HEADER_UUP_RUT, HEADER_UUP_UID, HEADER_FLAG_MIGRADO);

	    }
	    //LOGGER_SEG.info("------------------------------------------------- USARMGDB {}", usarMigraDB);

	    //LOGGER_SEG.info("Intentando validar usuario [{}] accediendo a [{}]", HEADER_UUP_RUT, request.getRequestURI());

	    
	    // Modificacion para Mock
	    HEADER_UUP_RUT = "0000000001-9";
	    HEADER_UUP_UID = "0000000001-9";


	    if (HEADER_UUP_RUT != null && HEADER_UUP_UID != null) 
	    {
		// Condicion para Mock //
	     	if(HEADER_UUP_RUT.equals(HEADER_UUP_UID))
	     	{
		    if(HEADER_UUP_RUT != null)
		    {
	  	    	HEADER_UUP_RUT=Utils.rutSinCeros(HEADER_UUP_RUT);
		    }

		    Usuario usuario = usuariosService.obtenerUsuario(HEADER_UUP_RUT);
		    // LOGGER_SEG.debug("Usuario obtenido [{}]", usuario);
		    user = new VTRUser(usuario);
		    user.setFechaMigracion(fechaMigracion);
		    user.setMigrado(migrado);
		    user.setUsuarioError(error);
		    user.addDetail(VTRUser.GENERAR_CONTACTO, request.getHeader(VTRUser.GENERAR_CONTACTO));
		    user.addDetail(VTRUser.DETALLE_CRM_USER, HEADER_APP_CONTACTO_CRM_USER);
		    user.addDetail(VTRUser.ORIGEN_SV, request.getHeader(VTRUser.ORIGEN_SV));
		    // https://logback.qos.ch/manual/mdc.html
		    //MDC.put(KEY_LOG_CLIENTE, HEADER_UUP_RUT);
	    	}

	    } 
	    else 
	    {
		log.info("No se pudo identificar al usuario [{}] acceso no permitido a [{}]", HEADER_UUP_RUT, request.getRequestURI());
		//LOGGER_SEG.warn("No se pudo identificar al usuario [{}] acceso no permitido a [{}]", HEADER_UUP_RUT, request.getRequestURI());
	    }

	    //log.info("VTRAuthenticationFilter.validate - end");
	    return user;
	}

	private void modificarProperties(HttpServletRequest request, boolean add) 
	{
	    @SuppressWarnings("unchecked")
	    Enumeration<String> headers = request.getHeaderNames();
	    if (headers != null) 
 	    {
	  	while (headers.hasMoreElements()) 
		{
		    String header = headers.nextElement();
		    if (header != null && header.toLowerCase().startsWith("property_")) 
		    {
		 	String head = header.substring(9).toLowerCase();
			if (add) 
			{
			    String valor = request.getHeader(header);
			    //LOGGER_SEG.debug("Modificando propiedad [{}][{}]", head, valor); 
			    //propertyService.setProperty(head, valor);
			} 
			else 
			{
			    //LOGGER_SEG.debug("Eliminando propiedad [{}]", head);
			    //propertyService.removeProperty(head);
			}
		    }
		}
	    }
	}
}
