package cl.agiled.vtr.smartwifi.security;

//import cl.karibu.commons.web.dto.comunes.ParNombreValor;
//import cl.karibu.commons.web.services.PropertyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 *
 * @author dbustamante
 */
@Configuration(value = "VTRSecurityConfig")
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class VTRSecurityConfig extends WebSecurityConfigurerAdapter 
{
    @Autowired
    private VTRAuthenticationProvider authProvider;
    
    @Autowired
    private VTRAuthenticationFilter filter;
    
    //@Autowired
    //private PropertyService propertyService;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception 
    {
        auth.authenticationProvider(authProvider);
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().addFilterBefore(filter, BasicAuthenticationFilter.class);
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        //web.ignoring().antMatchers("/*.json");
    	web.ignoring().antMatchers("/**");
    	/*List<ParNombreValor> publicUrls = this.propertyService.getListProperties("security.url.public");
        if (publicUrls != null) {
            for (ParNombreValor pnv: publicUrls) {
                web.ignoring().antMatchers(pnv.getValor());
            }
        }*/
    }
}
