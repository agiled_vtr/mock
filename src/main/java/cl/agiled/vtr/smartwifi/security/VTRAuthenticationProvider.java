package cl.agiled.vtr.smartwifi.security;

//import com.vtr.servicesvtr.dto.VTRUser;
//import com.vtr.servicesvtr.dto.comunes.usuarios.Usuario;
//import com.vtr.servicesvtr.services.usuarios.UsuariosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import cl.agiled.vtr.smartwifi.model.VTRUser;
import cl.agiled.vtr.smartwifi.model.Usuario;
import cl.agiled.vtr.smartwifi.services.UsuariosService;


/**
 *
 * @author dbustamante
 */
@Component
public class VTRAuthenticationProvider implements AuthenticationProvider 
{
    @Autowired
    private UsuariosService usuariosService;
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException 
    {
        OAMAuthentication auth = (OAMAuthentication) authentication;
        Usuario usuario = usuariosService.obtenerUsuario(auth.getName());
        if (usuario != null) 
	{
            return new VTRUser(usuario);
        }
        
        throw new SecurityException("Could not find user with rut: " + auth.getName());
    }

    @Override
    public boolean supports(Class<?> authentication) 
    {
        return authentication.equals(OAMAuthentication.class);
    }
    
}
