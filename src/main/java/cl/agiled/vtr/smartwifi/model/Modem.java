package cl.agiled.vtr.smartwifi.model;

import java.io.Serializable;

public class Modem implements Serializable
{
    private static final long SerialVersionUID = 1L;

    private String macAddress;

    public Modem()
    {	
	super();
    }

    public String getMacAddress()
    {
	return macAddress;
    }
    public void setMacAddress(String macAddress)
    {
	this.macAddress = macAddress;
    }

}
