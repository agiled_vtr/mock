package cl.agiled.vtr.smartwifi.model;

import java.io.Serializable;
import java.util.List;

public class AccountId implements Serializable
{
    private static final long SerialVersionUID = 1L;

    private String accountId;
    private String businessId;
    private List<String> addressLines;
    private List<DataService> dataServices;

    public AccountId()
    {	
	super();
    }

    public String getAccountId()
    {
	return accountId;
    }
    public void setAccountId(String accountId)
    {
	this.accountId = accountId;
    }

    public String getBusinessId()
    {
	return businessId;
    }
    public void setBusinessId(String businessId)
    {
	this.businessId = businessId;
    }

    public List<String> getAddressLines()
    {
	return addressLines;
    }
    public void setAddressLines(List<String> addressLines)
    {
	this.addressLines = addressLines;
    }


    public List<DataService> getDataServices()
    {
	return dataServices;
    }
    public void setDataServices(List<DataService> dataServices)
    {
	this.dataServices = dataServices;
    }

}
