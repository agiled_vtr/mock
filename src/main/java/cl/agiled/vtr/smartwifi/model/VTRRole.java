package cl.agiled.vtr.smartwifi.model;

import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author dbustamante
 */
public class VTRRole implements GrantedAuthority 
{
    private static final long serialVersionUID = -3466731403494252548L;
	
    private String nombre;
    
    @Override
    public String getAuthority() {
        return this.nombre;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
