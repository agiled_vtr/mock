package cl.agiled.vtr.smartwifi.model;

import java.io.Serializable;
import java.util.List;

public class DataService implements Serializable
{
    private static final long SerialVersionUID = 1L;

    private String serviceId;
    private String serviceDescription;
    private List<Modem> modems;

    public DataService()
    {	
	super();
    }

    public String getServiceId()
    {
	return serviceId;
    }
    public void setServiceId(String serviceId)
    {
	this.serviceId = serviceId;
    }

    public String getServiceDescription()
    {
	return serviceDescription;
    }
    public void setServiceDescription(String serviceDescription)
    {
	this.serviceDescription = serviceDescription;
    }
   
    public List<Modem> getModems()
    {
	return modems;
    }
    public void setModems(List<Modem> modems)
    {
	this.modems = modems;
    }
}
