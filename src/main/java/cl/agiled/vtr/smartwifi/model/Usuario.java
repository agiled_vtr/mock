package cl.agiled.vtr.smartwifi.model;

import java.io.Serializable;
import java.util.Calendar;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author druiz
 */
public class Usuario implements Serializable 
{
    private static final long serialVersionUID = -1386759888829295511L;
	
    private String actividadProfesion;
    private String apellidoMaterno;
    private String apellidoPaterno;
    private String celularDeContacto;
    private String checkAceptacionCondicionesDeUso;
    private String checkEnvioInformacionVTR;
    private String emailDeContacto;
    private String estadoCivil;
    private Calendar fechaCreacionComoCliente;
    private Calendar fechaDeNacimiento;
    private String fijoDeContacto;
    private String flagTipoPersona;
    private String genero;
    private String nombres;
    private String rut;
    private String usuario;
    private String calle;
    private String casaDeptoParcela;
    private String celularDePrepago;
    private String checkEnvioInformacionEquiposMoviles;
    private String checkEnvioInformacionRSEVTR;
    private String checkInfo1;
    private String checkInfo2;
    private String checkInfo3;
    private String checkInfo4;
    private String checkInfo5;
    private String ciudad;
    private String direccionDeContacto;
    private Calendar fechaAniversarioMatrimonio;
    private Calendar fechaUltimaModificacionDePerfil;
    private String localidad;
    private String numero;
    private String numeroCasaDeptoParce;
    private String region;

    public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCasaDeptoParcela() {
		return casaDeptoParcela;
	}

	public void setCasaDeptoParcela(String casaDeptoParcela) {
		this.casaDeptoParcela = casaDeptoParcela;
	}

	public String getCelularDePrepago() {
		return celularDePrepago;
	}

	public void setCelularDePrepago(String celularDePrepago) {
		this.celularDePrepago = celularDePrepago;
	}

	public String getCheckEnvioInformacionEquiposMoviles() {
		return checkEnvioInformacionEquiposMoviles;
	}

	public void setCheckEnvioInformacionEquiposMoviles(
			String checkEnvioInformacionEquiposMoviles) {
		this.checkEnvioInformacionEquiposMoviles = checkEnvioInformacionEquiposMoviles;
	}

	public String getCheckEnvioInformacionRSEVTR() {
		return checkEnvioInformacionRSEVTR;
	}

	public void setCheckEnvioInformacionRSEVTR(String checkEnvioInformacionRSEVTR) {
		this.checkEnvioInformacionRSEVTR = checkEnvioInformacionRSEVTR;
	}

	public String getCheckInfo2() {
		return checkInfo2;
	}

	public void setCheckInfo2(String checkInfo2) {
		this.checkInfo2 = checkInfo2;
	}

	public String getCheckInfo3() {
		return checkInfo3;
	}

	public void setCheckInfo3(String checkInfo3) {
		this.checkInfo3 = checkInfo3;
	}

	public String getCheckInfo4() {
		return checkInfo4;
	}

	public void setCheckInfo4(String checkInfo4) {
		this.checkInfo4 = checkInfo4;
	}

	public String getCheckInfo5() {
		return checkInfo5;
	}

	public void setCheckInfo5(String checkInfo5) {
		this.checkInfo5 = checkInfo5;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDireccionDeContacto() {
		return direccionDeContacto;
	}

	public void setDireccionDeContacto(String direccionDeContacto) {
		this.direccionDeContacto = direccionDeContacto;
	}

	public Calendar getFechaAniversarioMatrimonio() {
		return fechaAniversarioMatrimonio;
	}

	public void setFechaAniversarioMatrimonio(Calendar fechaAniversarioMatrimonio) {
		this.fechaAniversarioMatrimonio = fechaAniversarioMatrimonio;
	}

	public Calendar getFechaUltimaModificacionDePerfil() {
		return fechaUltimaModificacionDePerfil;
	}

	public void setFechaUltimaModificacionDePerfil(
			Calendar fechaUltimaModificacionDePerfil) {
		this.fechaUltimaModificacionDePerfil = fechaUltimaModificacionDePerfil;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroCasaDeptoParce() {
		return numeroCasaDeptoParce;
	}

	public void setNumeroCasaDeptoParce(String numeroCasaDeptoParce) {
		this.numeroCasaDeptoParce = numeroCasaDeptoParce;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getCelularDeContacto() {
        return celularDeContacto;
    }

    public void setCelularDeContacto(String celularDeContacto) {
        this.celularDeContacto = celularDeContacto;
    }

    public String getCheckAceptacionCondicionesDeUso() {
        return checkAceptacionCondicionesDeUso;
    }

    public void setCheckAceptacionCondicionesDeUso(String checkAceptacionCondicionesDeUso) {
        this.checkAceptacionCondicionesDeUso = checkAceptacionCondicionesDeUso;
    }

    public String getCheckEnvioInformacionVTR() {
        return checkEnvioInformacionVTR;
    }

    public void setCheckEnvioInformacionVTR(String checkEnvioInformacionVTR) {
        this.checkEnvioInformacionVTR = checkEnvioInformacionVTR;
    }

    public String getCheckInfo1() {
        return checkInfo1;
    }

    public void setCheckInfo1(String checkInfo1) {
        this.checkInfo1 = checkInfo1;
    }

    public String getEmailDeContacto() {
        return emailDeContacto;
    }

    public void setEmailDeContacto(String emailDeContacto) {
        this.emailDeContacto = emailDeContacto;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Calendar getFechaCreacionComoCliente() {
        return fechaCreacionComoCliente;
    }

    public void setFechaCreacionComoCliente(Calendar fechaCreacionComoCliente) {
        this.fechaCreacionComoCliente = fechaCreacionComoCliente;
    }

    public Calendar getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Calendar fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public String getFijoDeContacto() {
        return fijoDeContacto;
    }

    public void setFijoDeContacto(String fijoDeContacto) {
        this.fijoDeContacto = fijoDeContacto;
    }

    public String getFlagTipoPersona() {
        return flagTipoPersona;
    }

    public void setFlagTipoPersona(String flagTipoPersona) {
        this.flagTipoPersona = flagTipoPersona;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

	public String getActividadProfesion() {
		return actividadProfesion;
	}

	public void setActividadProfesion(String actividadProfesion) {
		this.actividadProfesion = actividadProfesion;
	}
}
