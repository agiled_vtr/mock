package cl.agiled.vtr.smartwifi.model;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;

//import com.vtr.servicesvtr.dto.comunes.usuarios.Usuario;

/**
 *
 * @author dbustamante
 */
public class VTRUser implements Authentication 
{
    private static final long serialVersionUID = 1L;

    private Map<String, Object> details;
	
    private boolean autenthicated;
    private String nombreCompleto;
    private Usuario usuario;
    private boolean migrado      = true;
    private boolean usuarioError = false;
    private long idThread;
    private Date fechaMigracion;

    private String identificador="0000000-0";
    
    public static final String DETALLE_CRM_USER = "_CONTACTO_CRM_APP_USER"; 
    public static final String GENERAR_CONTACTO = "HABILITAR_CONTACTO"; 
    public static final String ORIGEN_SV="ORIGEN_SV";
    
    public VTRUser(Usuario usuario) 
    {
        this.usuario = usuario;
        this.autenthicated = true;
        this.identificador=usuario.getRut();
        this.idThread=Thread.currentThread().getId();
    }
    
    public Date getFechaMigracion() 
    {
	return fechaMigracion;
    }

    public void setFechaMigracion(Date fechaMigracion) 
    {
 	this.fechaMigracion = fechaMigracion;
    }

    public String getIdentificador() 
    {
	return identificador;
    }

    public void setIdentificador(String identificador) 
    {
 	this.identificador = identificador;
    }
    
    public String getNombreCompleto() 
    {
	return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) 
    {
	this.nombreCompleto = nombreCompleto;
    }

    public long getIdThread() 
    {
	return idThread;
    }

    public void setIdThread(long idThread) 
    {
	this.idThread = idThread;
    }

    @Override
    public Collection<VTRRole> getAuthorities() 
    {
        return Collections.emptyList();
    }

    @Override
    public Object getCredentials() 
    {
        return null;
    }

    @Override
    public Object getDetails() 
    {
        return details;
    }
    
    public void addDetail(String name, Object value)
    {
    	if (details == null)
    	{
    	    details = new HashMap<String, Object>();
    	}
    	details.put(name, value);
    }
    
    @SuppressWarnings("unchecked")
    public <T> T getDetail(String name)
    {
    	T obj = null;
    	
    	Object value = details.get(name);
    	if(value != null)
	{
    	    obj = (T) value;
    	}
    	
    	return obj;
    }

    @Override
    public Usuario getPrincipal() 
    {
        return this.usuario;
    }

    @Override
    public boolean isAuthenticated() 
    {
        return autenthicated;
    }

    @Override
    public void setAuthenticated(boolean bln) throws IllegalArgumentException 
    {
        this.autenthicated = bln;
    }

    @Override
    public String getName() 
    {
        if (this.nombreCompleto == null) 
	{
            StringBuilder sb = new StringBuilder();
            if (this.usuario.getNombres() != null) 
	    {
                sb.append(this.usuario.getNombres());
            }
            if (usuario.getNombres() != null && usuario.getApellidoPaterno() != null) 
	    {
                sb.append(" ");
            }
            sb.append(this.usuario.getApellidoPaterno());
            if (this.usuario.getApellidoMaterno() != null) 
	    {
                sb.append(" ").append(this.usuario.getApellidoMaterno());
            }
            this.nombreCompleto = sb.toString();
        }

        return this.nombreCompleto;
    }

    public boolean isMigrado() 
    {
	return migrado;
    }

    public void setMigrado(boolean migrado) 
    {
 	this.migrado = migrado;
    }

    public boolean isUsuarioError() 
    {
	return usuarioError;
    }

    public void setUsuarioError(boolean usuarioError) 
    {
	this.usuarioError = usuarioError;
    }
}
