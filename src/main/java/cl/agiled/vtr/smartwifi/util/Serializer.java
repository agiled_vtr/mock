package cl.agiled.vtr.smartwifi.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Serializer 
{
    private static Logger log = LoggerFactory.getLogger(Serializer.class);

    public static byte[] serialize(Object obj) 
    {
	if (obj == null) 
	{
	    return null;
	}

	ByteArrayOutputStream bos = new ByteArrayOutputStream();
	ObjectOutput out = null;
	byte[] arr = null;

	try 
	{
	    out = new ObjectOutputStream(bos);
	    out.writeObject(obj);
	    out.flush();
	    arr = bos.toByteArray();
	} 
	catch (IOException e) 
	{
	    log.error("Error to serialize object", e);
	} 
	finally 
	{
	    try 
	    {
		bos.close();
	    } 
	    catch (IOException ex) 
	    {
		// ignore close exception
	    }
	}

	return arr;
    }
}
