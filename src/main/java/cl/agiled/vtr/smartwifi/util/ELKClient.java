package cl.agiled.vtr.smartwifi.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonInclude;

import cl.agiled.vtr.smartwifi.util.ElasticLogger;

public class ELKClient 
{
	private static Logger log = LoggerFactory.getLogger(ELKClient.class);
	
	private static final String DOC_NOTIFICATION = "event";
	private static SimpleDateFormat sdfTS        = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	private static SimpleDateFormat sdfIDX       = new SimpleDateFormat("yyyyMMdd");
	private static ObjectMapper objectMapper     = new ObjectMapper();
	
	private static String cleanNull(String s) 
	{
		if (s!=null) 
		{
			s = s.replace("\"[A-Za-z]+\":\"null\"","");
			s = s.replace("\"[A-Za-z]+\":null","");
		}
		return s;
	}

	public static void notifyELK(String messageId, String type, Object data) 
	{
            try 
	    {
            	if (data instanceof java.lang.String)
		{
                    notifyMessage(messageId,type,cleanNull((String)data));
		}
            	else 
		{
		    String json = objectMapper.writeValueAsString(data);
                    notifyMessage(messageId,type,cleanNull(json));
		}
            } 
	    catch (Exception e) 
	    {
	    }
    	}

	private static void notifyMessage(String messageId, String type, String data) 
	{
	    try 
	    {
		log.debug("Sending message to Elastic {} {}",type,data);
		if (data!=null && !data.startsWith("{")) 
		{ 
		    data = "{ \"message\":\""+data+"\" }";
		}
		if (data!=null) 
		{
		    JsonNode json = createMessage(messageId, type, data);
		    sendMessage(json);
		} 
		else 
		{
		    log.warn("Error ELK {} {} {}",messageId,type,data);
		}
	     }
	     catch (Exception e) 
	     {
		log.warn("Error ELK {} {} {}",messageId,type,data,e);
	     }
	}

	private static void sendMessage(JsonNode json) 
	{
	    log.trace("Sending message to Elastic");
		
	    try 
 	    {
		if (json != null) 
		{
		    log.debug(json.toString());
		    ElasticLogger.notifyTraceMessage(json.toString());
		    //log.trace(json.toString());
		}	
	    }
	    catch (Exception e) 
	    {
		log.error("Error to send message to ELK", e);
	    }
	}
	
	private static JsonNode createMessage(String messageId, String type, String data) 
	{
	    try 
	    {
		JsonNode rootNode = objectMapper.createObjectNode();
		JsonNode headerNode = objectMapper.createObjectNode();
		((ObjectNode) headerNode).put("application", "isuscribe-service");
		((ObjectNode) headerNode).put("timestamp", sdfTS.format(new Date()));
		((ObjectNode) headerNode).put("messageId", messageId);
		((ObjectNode) headerNode).put("type", type);
		((ObjectNode) rootNode).set("header", headerNode);

		JsonNode dataNode = objectMapper.readTree(data);
		((ObjectNode) rootNode).put("data", dataNode);

		return rootNode;
	    } 
	    catch (Exception e) 
	    {
		log.error("Error to create JSON message {}",e.getMessage());
		return null;
	    }
	}

	public static String getID()
	{
	    String pattern = "yyyyMMddHHmmssSSS";
	    SimpleDateFormat simpleDateFormat =  new SimpleDateFormat(pattern);
	    return simpleDateFormat.format(new Date());
	}
}
