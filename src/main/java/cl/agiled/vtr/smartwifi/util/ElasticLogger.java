package cl.agiled.vtr.smartwifi.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElasticLogger 
{
	private static final Logger log = LoggerFactory.getLogger(ElasticLogger.class);

	public static void notifyTraceMessage(String data) 
	{
	    log.trace(data);
	}
}
