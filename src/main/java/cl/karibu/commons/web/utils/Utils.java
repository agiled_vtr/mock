package cl.karibu.commons.web.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Utils 
{
        private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        private static final String PATTERN_RUT = "^[0-9]+-[0-9kK]{1}$";

        private static final String EMAIL_DELIM = "@";

        private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

        public static final String SET_CARACTERES_ESPECIALES_CONTRASENA = "!@#$%^&*()_+";
        private static final String CODIGO_ERROR_GENERACION_CONTRASENA="CARACTERES_ESPECIALES_ERRADOS";

        private Utils() 
	{

        }


        public static String desformateaRut(String rut) {
                String aux = null;
                if (rut != null && !rut.isEmpty()) {
                        aux = rut.replaceAll("[.-]", "");
                        aux = aux.replaceAll("^[0]+", "");
                        aux = aux.substring(0, aux.length() - 1);
                } else if (rut != null && rut.isEmpty()) {
                        return rut;
                }

                return aux;
        }

        public static String formateaRut(String rut) {
                try {
                        if (rut != null && !rut.isEmpty()) {
                                long rutNumerico = Long.parseLong(rut);
                                String rutFormateado = NumberFormat.getInstance(new Locale("ES", "CL")).format(rutNumerico);
                                return rutFormateado + "-" + calculaDV(rutNumerico).toUpperCase();
                        }
                        return null;
                } catch (Exception ex) {
                        // FIXME: stacktrace cambiar por logger
                        ex.printStackTrace();
                }

                return "";
        }

        public static String rutSinCeros(String rut) {
                if (rut != null && !rut.isEmpty()) {
                        rut = rut.replaceAll("^[0]+", "");
                } else if (rut != null && rut.isEmpty()) {
                        return rut;
                }

                return rut;
        }

        public static String calculaDV(long rut) {
                int module = 0;
                long digito = 1;
                for (; rut != 0; rut /= 10) {
                        digito = (digito + rut % 10 * (9 - module++ % 6)) % 11;
                }
                return "" + ((char) (digito != 0 ? digito + 47 : 75));
        }

        public static boolean validaEmail(String email) {
                Pattern pattern = Pattern.compile(PATTERN_EMAIL);

                // Match the given input against this pattern
                Matcher matcher = pattern.matcher(email);
                return matcher.matches();
        }

        public static String getEmailUser(String email) {
                String str = StringUtils.EMPTY;

                String[] temp;
                temp = email.split(EMAIL_DELIM);

                if (temp != null && temp.length > 0) {
                        str = temp[0];
                }

                return str;
        }
        public static String getEmailMasked(String email) {
                if(!validaEmail(email)) {
                        throw new IllegalArgumentException("email con formato inválido");
                }

                StringBuilder makedEmailUser = new StringBuilder();
                String emailUser=getEmailUser(email);
                makedEmailUser.append(emailUser.charAt(0));
                for (int i=1;i<emailUser.length()-1;i++){
                        makedEmailUser.append("x");
                }
                if(emailUser.length()>1){
                        makedEmailUser.append(emailUser.charAt(emailUser.length()-1));
                }
                makedEmailUser.append("@");
                makedEmailUser.append(getEmailDomain(email));

                return makedEmailUser.toString();
        }

        public static String getEmailDomain(String email) {
                String str = StringUtils.EMPTY;

                String[] temp;
                temp = email.split(EMAIL_DELIM);

                if (temp != null && temp.length > 1) {
                        str = temp[1];
                }

                return str;
        }

        public static String getStringWithoutSpecialCharacters(String cadena) {
                cadena = cadena.replaceAll("[^A-Za-z0-9áéíóúÁÉÍÓÚñÑ\\s]", " ");
                cadena = cadena.trim();
                return cadena;
        }


        public static String stackToString(Exception e) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                String sStackTrace = sw.toString(); // stack trace as a string
                return sStackTrace;
        }

        public static boolean validaRut(String rut) {
                Pattern pattern = Pattern.compile(PATTERN_RUT);

                // Match the given input against this pattern
                Matcher matcher = pattern.matcher(rut);
                return matcher.matches();
        }

        public static char entregaDigVerif(String nmro_rut) {
                int M = 0, S = 1, T = Integer.parseInt(nmro_rut);
                for (; T != 0; T /= 10) {
                        S = (S + T % 10 * (9 - M++ % 6)) % 11;
                }
                return (char) (S != 0 ? S + 47 : 75);

        }

        public static String eliminarCerosIzquierda(String input) {
                if (input.length() > 0 && input.charAt(0) == '0') {
                        return eliminarCerosIzquierda(input.substring(1));
                } else {
                        return input;
                }
        }

        /*
         * Validar formato de un RUT y su dígito verificador.
         * <p>
         * método que valida si el el RUT enviado por parametro cumple con el formato respectivo y si
         * el dígito verificador es el correcto.
         * </p>
         * @param rut RUT
         * @return boolean resultado de la validación del RUT
         * */
        public static boolean validarRutYDigitoVerificador(String rut) {
                boolean validacion = false;

                try {
//                      StringBuilder sbRut = new StringBuilder(rut.toUpperCase());
                        rut = rut.toUpperCase();
//                      sbRut.toString().replaceAll(".", "");
                        rut = rut.replace(".", "");
//                      sbRut.toString().replaceAll("-", "");
                        rut = rut.replace("-", "");
                        int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
//                      int rutAux = Integer.parseInt(sbRut.substring(0, sbRut.length() - 1));

                        char dv = rut.charAt(rut.length() - 1);
//                      char dv = sbRut.charAt(sbRut.length() - 1);

                        int m = 0, s = 1;
                        for (; rutAux != 0; rutAux /= 10) {
                                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                        }
                        if (dv == (char) (s != 0 ? s + 47 : 75)) {
                                validacion = true;
                        }

                } catch (java.lang.NumberFormatException e) {
                        validacion=false;
                } catch (Exception e) {
                        validacion=false;
                }
                return validacion;
        }


}
