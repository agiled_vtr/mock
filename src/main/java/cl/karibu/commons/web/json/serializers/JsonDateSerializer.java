package cl.karibu.commons.web.json.serializers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class JsonDateSerializer extends JsonSerializer<Date> {

	// ISO 8601
	public static final String PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(PATTERN);

	@Override
	public void serialize(Date date, JsonGenerator gen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		String formattedDate = null;

		if (date != null) {
			formattedDate = DATE_FORMAT.format(date);
		}

		gen.writeString(formattedDate);
	}
}
