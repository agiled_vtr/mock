/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.karibu.commons.web.json.serializers;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.ApplicationContext;

import cl.karibu.commons.logger.LoggerName;
import cl.karibu.commons.web.context.ApplicationContextHolder;
import cl.karibu.commons.web.dto.Messages;
import cl.karibu.commons.web.services.PropertyService;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 *
 * @author dbustamante
 */
public class MessagesSerializer extends JsonSerializer<Map<String, Messages>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoggerName.COMMON);

	@Autowired
	private PropertyService servicesvtr;

	public MessagesSerializer() {
		AutowiredAnnotationBeanPostProcessor bpp = new AutowiredAnnotationBeanPostProcessor();

		ApplicationContext context = ApplicationContextHolder.getContext();

		bpp.setBeanFactory(context.getAutowireCapableBeanFactory());
		bpp.processInjection(this);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void serialize(Map messages, JsonGenerator jg, SerializerProvider sp)
			throws IOException, JsonProcessingException {
		if (messages != null && !messages.isEmpty()) {
			Map<String, Map<String, List<String>>> output = new HashMap<String, Map<String, List<String>>>();

			for (String type : (Set<String>) messages.keySet()) {
				Map<String, Object[]> properties = (Map<String, Object[]>) messages.get(type);
				if (properties != null && !properties.isEmpty()) {
					for (String property : properties.keySet()) {
						List<String> msgs = (List<String>) properties.get(property)[0];
						List<Object[]> params = (List<Object[]>) properties.get(property)[1];

						if (msgs != null && !msgs.isEmpty()) {
							for (int i = 0; i < msgs.size(); i++) {
								this.addMessage(output, type, property, msgs.get(i), params.get(i));
							}
						}
					}
				}
			}

			jg.writeObject(output);
		}
	}

	private void addMessage(Map<String, Map<String, List<String>>> messages, String type,
			String property, String msg, Object[] params) {
		Map<String, List<String>> properties = (Map<String, List<String>>) messages.get(type);
		if (properties == null) {
			properties = new HashMap<String, List<String>>();
			messages.put(type, properties);
		}

		List<String> msgs = (List<String>) properties.get(property);
		if (msgs == null) {
			msgs = new ArrayList<String>();
			properties.put(property, msgs);
		}

		try {
			String aux = this.servicesvtr.getProperty(msg);
			if (aux == null) {
				aux = msg;
			}
			if (params == null || params.length == 0) {
				msgs.add(aux);
			} else {
				MessageFormat mf = new MessageFormat(aux);
				msgs.add(mf.format(params));
			}
		} catch (Exception ex) {
			LOGGER.error("ERROR Serializando mensajes", ex);
		}
	}
}
