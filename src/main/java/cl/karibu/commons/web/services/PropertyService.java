/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.karibu.commons.web.services;

import cl.karibu.commons.web.dto.comunes.ParNombreValor;
import java.util.List;

/**
 *
 * @author dbustamante
 */
public interface PropertyService {
	String getProperty(String propertyName);

	Integer getIntProperty(String propertyName);
	
	Integer getIntPropertyDefault(String propertyName, Integer _default);
	
	Integer getIntPropertyDefaultNonZero(String propertyName, Integer _default);

	Long getLongProperty(String propertyName);

	Boolean getBoolProperty(String propertyName);

	List<ParNombreValor> getListProperties(String propertyName);

	java.util.Map<String, String> getMapProperties(String propertyName);

	/**
	 * Funcion que busca una variable con valores separados por pipe (|) y devuelve una lista
	 * 
	 * @param propertyName
	 * @return
	 */
	List<String> getListValues(String propertyName);

	List<Integer> getListIntValues(String propertyName);

	void setProperty(String propertyName, String propertyValue);

	void removeProperty(String propertyName);
}