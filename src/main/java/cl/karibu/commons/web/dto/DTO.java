/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.karibu.commons.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;

import cl.karibu.commons.web.json.serializers.MessagesSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 *
 * @author dbustamante
 */
public class DTO<T> implements Serializable {

	private static final long serialVersionUID = -6708449547248974364L;

	private int status;
	private T data;
	@JsonSerialize(using = MessagesSerializer.class)
	private Map<String, Map<String, Object[]>> messages;
	private Map<String, Object> debugInfo;

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	public Map<String, Map<String, Object[]>> getMessages() {
		return this.messages;
	}

	public void setMessages(Map<String, Map<String, Object[]>> messages) {
		this.messages = messages;
	}

	public void addMessage(String type, String message) {
		this.addMessage(type, null, message);
	}

	public void addMessage(String type, String property, String message) {
		this.addMessage(type, property, message, null);
	}

	@SuppressWarnings("unchecked")
	public void addMessage(String type, String property, String message, Object[] params) {
		if (this.messages == null) {
			this.messages = new HashMap<String, Map<String, Object[]>>();
		}

		Map<String, Object[]> types = (Map<String, Object[]>) this.messages.get(type);
		if (types == null) {
			types = new HashMap<String, Object[]>();
			this.messages.put(type, types);
		}

		if (property == null) {
			property = "" + types.size();
		}

		Object[] msgs = (Object[]) types.get(property);
		if (msgs == null) {
			msgs = new Object[2];
			msgs[0] = new ArrayList<String>();
			msgs[1] = new ArrayList<Object[]>();
			types.put(property, msgs);
		}

		((List<String>) msgs[0]).add(message);
		((List<Object[]>) msgs[1]).add(params);
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public Map<String, Object> getDebugInfo() {
		return debugInfo;
	}

	public void setDebugInfo(Map<String, Object> debugInfo) {
		this.debugInfo = debugInfo;
	}

	public void addDebugInfo(String key, Object val) {
		if (this.debugInfo == null) {
			this.debugInfo = new HashMap<String, Object>();
		}
		debugInfo.put(key, val);
	}
}
