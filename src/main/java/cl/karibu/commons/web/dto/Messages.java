/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.karibu.commons.web.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dbustamante
 */
public class Messages {
    private Map<String, List<String>> messages;

    /**
     * @return the messages
     */
    public Map<String, List<String>> getMessages() {
        if (this.messages == null) {
            this.messages = new HashMap<String, List<String>>();
        }
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(Map<String, List<String>> messages) {
        this.messages = messages;
    }

    public void addMessage(String path, String message) {
        this.addMessage(path, message, null);
    }
    
    public void addMessage(String path, String message, Object[] params) {
        
        List<String> msgs = this.getMessages().get(path);
        if (msgs == null) {
            msgs = new ArrayList<String>();
        }
        
        msgs.add(message);
        this.messages.put(path, msgs);
    }
}
