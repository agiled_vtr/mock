/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.karibu.commons.web.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cl.karibu.commons.logger.LoggerName;

/**
 *
 * @author dbustamante
 */
@Component
@Lazy(value=false)
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ApplicationContextHolder implements ApplicationContextAware {
    protected static ApplicationContext context;
    protected Logger LOGGER = LoggerFactory.getLogger(LoggerName.COMMON);

    @Override
    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;   
    }

    public static ApplicationContext getContext() {
        return context;
    }
}
