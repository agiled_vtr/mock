package cl.karibu.commons.logger;

public interface LoggerName {

	/**
	 * Service Client / Factory
	 */
	public static final String SERVICES = "services";
	public static final String APPLICATION = "application";
	/**
	 * Controller / Delegate
	 */
	public static final String WEB = "web";
	
	public static final String PERFORMANCE = "performance";
	public static final String SEGURIDAD = "seguridad";
	public static final String COMMON = "common";
	public static final String UI = "ui";
	public static final String ERRORS="errors";

}
