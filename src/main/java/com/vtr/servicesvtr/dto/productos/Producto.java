package com.vtr.servicesvtr.dto.productos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cl.karibu.commons.web.json.serializers.JsonDateSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Producto implements Serializable {

	private static final long serialVersionUID = -330797558368883593L;
	
	private String idCuenta;
	private String codigo;
	private String nbProducto;
	private String descripcion;
	private boolean flagPrepago;
	private BigDecimal monto;
	private String tipoPlan;
	private String tipo;
	private String tipoIdTitular;
	private String numeroPieza;
	private String nombreCuenta;
	private String nombrePlan; // caso espacial de TOBE para poder saber si es CC, SS O PP, Por la fusión de promociones.
	
	public String getNumeroPieza() {
		return numeroPieza;
	}

	public void setNumeroPieza(String numeroPieza) {
		this.numeroPieza = numeroPieza;
	}

	public String getTipoIdTitular() {
		return tipoIdTitular;
	}

	public void setTipoIdTitular(String tipoIdTitular) {
		this.tipoIdTitular = tipoIdTitular;
	}

	private int idServicio;// AGREGADO PARA ASIS
	
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date fechaVencimiento;
	private String urlPago;
	private List<DetalleProducto> detalles;
	private boolean poseeInternet; 
	private boolean poseeTelefonia;
	private boolean poseeTelevision;
	private boolean productoMovil;
	private boolean productoMovilBAM;
	private boolean suspendido;
	private boolean statusMovilActivo;

	public String getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(String idCuenta) {
		this.idCuenta = idCuenta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNbProducto() {
		return nbProducto;
	}

	public void setNbProducto(String nbProducto) {
		this.nbProducto = nbProducto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<DetalleProducto> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetalleProducto> detalles) {
		this.detalles = detalles;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getUrlPago() {
		return urlPago;
	}

	public void setUrlPago(String urlPago) {
		this.urlPago = urlPago;
	}

	public boolean isFlagPrepago() {
		return flagPrepago;
	}

	public void setFlagPrepago(boolean flagPrepago) {
		this.flagPrepago = flagPrepago;
	}

	public String getTipoPlan() {
		return tipoPlan;
	}

	public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

	public boolean isPoseeInternet() {
		return poseeInternet;
	}

	public void setPoseeInternet(boolean poseeInternet) {
		this.poseeInternet = poseeInternet;
	}

	public boolean isPoseeTelefonia() {
		return poseeTelefonia;
	}

	public void setPoseeTelefonia(boolean poseeTelefonia) {
		this.poseeTelefonia = poseeTelefonia;
	}

	public boolean isPoseeTelevision() {
		return poseeTelevision;
	}

	public void setPoseeTelevision(boolean poseeTelevision) {
		this.poseeTelevision = poseeTelevision;
	}

	public boolean isProductoMovil() {
		return productoMovil;
	}

	public void setProductoMovil(boolean productoMovil) {
		this.productoMovil = productoMovil;
	}

	public boolean isSuspendido() {
		return suspendido;
	}

	public void setSuspendido(boolean suspendido) {
		this.suspendido = suspendido;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public boolean isStatusMovilActivo() {
		return statusMovilActivo;
	}
	public void setStatusMovilActivo(boolean statusMovilActivo) {
		this.statusMovilActivo = statusMovilActivo;
	}

	public boolean isProductoMovilBAM() {
		return productoMovilBAM;
	}

	public void setProductoMovilBAM(boolean productoMovilBAM) {
		this.productoMovilBAM = productoMovilBAM;
	}

	public String getNombreCuenta() {
		return nombreCuenta;
	}

	public void setNombreCuenta(String nombreCuenta) {
		this.nombreCuenta = nombreCuenta;
	}

	public String getNombrePlan() {
		return nombrePlan;
	}

	public void setNombrePlan(String nombrePlan) {
		this.nombrePlan = nombrePlan;
	} 

}
