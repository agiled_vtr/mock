package com.vtr.servicesvtr.dto.productos;

import java.io.Serializable;

public class DetalleProducto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombre;
	private String tipo;
	private String idServicio;

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
