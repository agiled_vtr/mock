package com.vtr.servicesvtr.dto.boleta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Activo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codigoPadre;
	private String codigo;
	private String nombre;
	private String descripcion;
	private String cdContrato;
	private int idServicio;// AGREGADO PARA ASIS
	private Integer corrConcepto;
	private Integer idGrupoTar;
	private String xCodiClasserv;

	private String mercados; //Tipo plan
	private String tipoMovil;
	private String estadoMovil;
	private String plan;
	private String tipoAsociacion;
	private Integer idenOpcionNet;
	private Float cuotaNavegacion;
	private Float cuotaVOZ;
	private Float cuotaSMS;
	private Float cuotaMMS;
	private String claseProducto;
	private String identificadorProductoPrincipal;
	private String identificadorPromocionPrincipal;
	private String identificadorPromocionActivo;
	private String identificadorIntegracion;
	private String direccion;
	private String familia;
	private String nivelEmpaquetado;
	private String CuentaServicio;
	private String direccionGis;
	private String descripcionActivo;
	private String tipoProducto;
	
	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getDireccionGis() {
		return direccionGis;
	}

	public void setDireccionGis(String direccionGis) {
		this.direccionGis = direccionGis;
	}

	public String getCuentaServicio() {
		return CuentaServicio;
	}

	public void setCuentaServicio(String cuentaServicio) {
		CuentaServicio = cuentaServicio;
	}

	public String getNivelEmpaquetado() {
		return nivelEmpaquetado;
	}

	public void setNivelEmpaquetado(String nivelEmpaquetado) {
		this.nivelEmpaquetado = nivelEmpaquetado;
	}

	public String getFamilia() {
		return familia;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getIdentificadorIntegracion() {
		return identificadorIntegracion;
	}

	public void setIdentificadorIntegracion(String identificadorIntegracion) {
		this.identificadorIntegracion = identificadorIntegracion;
	}

	public String getIdentificadorPromocionActivo() {
		return identificadorPromocionActivo;
	}

	public void setIdentificadorPromocionActivo(String identificadorPromocionActivo) {
		this.identificadorPromocionActivo = identificadorPromocionActivo;
	}

	private String identificadorProducto;
	private String categoriaDetallada;
	private String identificadorGis;
	private String numeroMovil;
	private String promocion;
	private String status;
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPromocion() {
		return promocion;
	}

	public void setPromocion(String promocion) {
		this.promocion = promocion;
	}

	public String getNumeroMovil() {
		return numeroMovil;
	}

	public void setNumeroMovil(String numeroMovil) {
		this.numeroMovil = numeroMovil;
	}

	public String getIdentificadorGis() {
		return identificadorGis;
	}

	public void setIdentificadorGis(String identificadorGis) {
		this.identificadorGis = identificadorGis;
	}

	public String getCategoriaDetallada() {
		return categoriaDetallada;
	}

	public void setCategoriaDetallada(String categoriaDetallada) {
		this.categoriaDetallada = categoriaDetallada;
	}

	public String getIdentificadorProducto() {
		return identificadorProducto;
	}

	public void setIdentificadorProducto(String identificadorProducto) {
		this.identificadorProducto = identificadorProducto;
	}

	public String getIdentificadorProductoPrincipal() {
		return identificadorProductoPrincipal;
	}

	public void setIdentificadorProductoPrincipal(
			String identificadorProductoPrincipal) {
		this.identificadorProductoPrincipal = identificadorProductoPrincipal;
	}

	public String getIdentificadorPromocionPrincipal() {
		return identificadorPromocionPrincipal;
	}

	public void setIdentificadorPromocionPrincipal(
			String identificadorPromocionPrincipal) {
		this.identificadorPromocionPrincipal = identificadorPromocionPrincipal;
	}

	public String getClaseProducto() {
		return claseProducto;
	}

	public void setClaseProducto(String claseProducto) {
		this.claseProducto = claseProducto;
	}

	private String numeroPieza;
	public String getNumeroPieza() {
		return numeroPieza;
	}

	public void setNumeroPieza(String numeroPieza) {
		this.numeroPieza = numeroPieza;
	}

	private List<Activo> activosSecundarios;

	public Activo() {
		activosSecundarios = new ArrayList<Activo>();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Activo> getActivosSecundarios() {
		return activosSecundarios;
	}

	public void setActivosSecundarios(List<Activo> activosSecundarios) {
		this.activosSecundarios = activosSecundarios;
	}

	public String getCodigoPadre() {
		return codigoPadre;
	}

	public void setCodigoPadre(String codigoPadre) {
		this.codigoPadre = codigoPadre;
	}

	public void addActivoSecundario(Activo activo) {
		if (this.activosSecundarios == null) {
			this.activosSecundarios = new ArrayList<Activo>();
		}

		this.activosSecundarios.add(activo);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getCorrConcepto() {
		return corrConcepto;
	}

	public void setCorrConcepto(Integer corrConcepto) {
		this.corrConcepto = corrConcepto;
	}

	public Integer getIdGrupoTar() {
		return idGrupoTar;
	}

	public void setIdGrupoTar(Integer idGrupoTar) {
		this.idGrupoTar = idGrupoTar;
	}

	public String getxCodiClasserv() {
		return xCodiClasserv;
	}

	public void setxCodiClasserv(String xCodiClasserv) {
		this.xCodiClasserv = xCodiClasserv;
	}

	public String getMercados() {
		return mercados;
	}

	public void setMercados(String mercados) {
		this.mercados = mercados;
	}

	public String getTipoMovil() {
		return tipoMovil;
	}

	public void setTipoMovil(String tipoMovil) {
		this.tipoMovil = tipoMovil;
	}

	public String getEstadoMovil() {
		return estadoMovil;
	}

	public void setEstadoMovil(String estadoMovil) {
		this.estadoMovil = estadoMovil;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getTipoAsociacion() {
		return tipoAsociacion;
	}

	public void setTipoAsociacion(String tipoAsociacion) {
		this.tipoAsociacion = tipoAsociacion;
	}

	public Integer getIdenOpcionNet() {
		return idenOpcionNet;
	}

	public void setIdenOpcionNet(Integer idenOpcionNet) {
		this.idenOpcionNet = idenOpcionNet;
	}

	public Float getCuotaNavegacion() {
		return cuotaNavegacion;
	}

	public void setCuotaNavegacion(Float cuotaNavegacion) {
		this.cuotaNavegacion = cuotaNavegacion;
	}

	public Float getCuotaVOZ() {
		return cuotaVOZ;
	}

	public void setCuotaVOZ(Float cuotaVOZ) {
		this.cuotaVOZ = cuotaVOZ;
	}

	public Float getCuotaSMS() {
		return cuotaSMS;
	}

	public void setCuotaSMS(Float cuotaSMS) {
		this.cuotaSMS = cuotaSMS;
	}

	public Float getCuotaMMS() {
		return cuotaMMS;
	}

	public void setCuotaMMS(Float cuotaMMS) {
		this.cuotaMMS = cuotaMMS;
	}

	public String getCdContrato() {
		return cdContrato;
	}

	public void setCdContrato(String cdContrato) {
		this.cdContrato = cdContrato;
	}

	public String getDescripcionActivo() {
		return descripcionActivo;
	}

	public void setDescripcionActivo(String descripcionActivo) {
		this.descripcionActivo = descripcionActivo;
	}

	
	
}
