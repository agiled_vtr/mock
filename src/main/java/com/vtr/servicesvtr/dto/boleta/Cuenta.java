package com.vtr.servicesvtr.dto.boleta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.vtr.servicesvtr.dto.direccion.Direccion;

public class Cuenta implements Serializable 
{
	private static final long serialVersionUID = 1L;

	private String codigo;
	
	private String historicoPago;

	private String nbProducto;

	private String nbPack;

	private boolean poseePAT;

	private boolean poseeBoletaElectronica;

	private boolean cuentaMovil;

	private BigDecimal deuda;

	private String nbCuenta;

	private String direccion;

	private String idDireccionGIS;

	private String identificadorServicio;

	private String correoElectronico;

	private String idFacturacion;

	private boolean suspendido;

	private boolean poseeTelefonia;
	private boolean poseeTelevision;
	private boolean poseeInternet;
	private boolean poseePremiun;

	private boolean fallaTelefonia;
	private boolean fallaTelevision;
	private boolean fallaInternet;
	private boolean fallaTelefoniaMovil;

	private String urlPagoCuenta;
	
	private String tipoPlan;

	private Boleta ultimaBoleta;

	private List<Activo> activos;

	private Direccion datosDireccion;

	private List<Boleta> deudaBoletas;

	public boolean isPoseeTelefonia() {
		return poseeTelefonia;
	}

	public void setPoseeTelefonia(boolean poseeTelefonia) {
		this.poseeTelefonia = poseeTelefonia;
	}

	public boolean isPoseeTelevision() {
		return poseeTelevision;
	}

	public void setPoseeTelevision(boolean poseeTelevision) {
		this.poseeTelevision = poseeTelevision;
	}

	public boolean isPoseeInternet() {
		return poseeInternet;
	}

	public void setPoseeInternet(boolean poseeInternet) {
		this.poseeInternet = poseeInternet;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNbProducto() {
		return nbProducto;
	}

	public void setNbProducto(String nbProducto) {
		this.nbProducto = nbProducto;
	}

	public boolean isPoseePAT() {
		return poseePAT;
	}

	public void setPoseePAT(boolean stSuscribirPAT) {
		this.poseePAT = stSuscribirPAT;
	}

	public boolean isPoseeBoletaElectronica() {
		return poseeBoletaElectronica;
	}

	public void setPoseeBoletaElectronica(boolean stSuscribirBoletaELectronica) {
		this.poseeBoletaElectronica = stSuscribirBoletaELectronica;
	}

	public String getNbCuenta() {
		return nbCuenta;
	}

	public void setNbCuenta(String nbCuenta) {
		this.nbCuenta = nbCuenta;
	}

	public BigDecimal getDeuda() {
		return deuda;
	}

	public void setDeuda(BigDecimal deuda) {
		this.deuda = deuda;
	}

	public boolean isCuentaMovil() {
		return cuentaMovil;
	}

	public void setCuentaMovil(boolean cuentaMovil) {
		this.cuentaMovil = cuentaMovil;
	}

	public String getNbPack() {
		return nbPack;
	}

	public void setNbPack(String nbPack) {
		this.nbPack = nbPack;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getIdentificadorServicio() {
		return identificadorServicio;
	}

	public void setIdentificadorServicio(String identificadorServicio) {
		this.identificadorServicio = identificadorServicio;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Boleta getUltimaBoleta() {
		return ultimaBoleta;
	}

	public void setUltimaBoleta(Boleta ultimaBoleta) {
		this.ultimaBoleta = ultimaBoleta;
	}

	public String getIdFacturacion() {
		return idFacturacion;
	}

	public void setIdFacturacion(String idFacturacion) {
		this.idFacturacion = idFacturacion;
	}

	public String getIdDireccionGIS() {
		return idDireccionGIS;
	}

	public void setIdDireccionGIS(String idDireccionGIS) {
		this.idDireccionGIS = idDireccionGIS;
	}

	public String getUrlPagoCuenta() {
		return urlPagoCuenta;
	}

	public void setUrlPagoCuenta(String urlPagoCuenta) {
		this.urlPagoCuenta = urlPagoCuenta;
	}

	public List<Activo> getActivos() {
		return activos;
	}

	public void setActivos(List<Activo> activos) {
		this.activos = activos;
	}

	public Direccion getDatosDireccion() {
		return datosDireccion;
	}

	public void setDatosDireccion(Direccion datosDireccion) {
		this.datosDireccion = datosDireccion;
	}

	public String getTipoPlan() {
		return tipoPlan;
	}

	public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

	public List<Boleta> getDeudaBoletas() {
		return deudaBoletas;
	}

	public void setDeudaBoletas(List<Boleta> deudaBoletas) {
		this.deudaBoletas = deudaBoletas;
	}

	public boolean isFallaTelefonia() {
		return fallaTelefonia;
	}

	public void setFallaTelefonia(boolean fallaTelefonia) {
		this.fallaTelefonia = fallaTelefonia;
	}

	public boolean isFallaTelevision() {
		return fallaTelevision;
	}

	public void setFallaTelevision(boolean fallaTelevision) {
		this.fallaTelevision = fallaTelevision;
	}

	public boolean isFallaInternet() {
		return fallaInternet;
	}

	public void setFallaInternet(boolean fallaInternet) {
		this.fallaInternet = fallaInternet;
	}

	public boolean isFallaTelefoniaMovil() {
		return fallaTelefoniaMovil;
	}

	public void setFallaTelefoniaMovil(boolean fallaTelefoniaMovil) {
		this.fallaTelefoniaMovil = fallaTelefoniaMovil;
	}

	public boolean isSuspendido() {
		return suspendido;
	}

	public void setSuspendido(boolean suspendido) {
		this.suspendido = suspendido;
	}

	public boolean isPoseePremiun() {
		return poseePremiun;
	}

	public void setPoseePremiun(boolean poseePremiun) {
		this.poseePremiun = poseePremiun;
	}

	public String getHistoricoPago() {
		return historicoPago;
	}

	public void setHistoricoPago(String historicoPago) {
		this.historicoPago = historicoPago;
	}

}
