package com.vtr.servicesvtr.dto.boleta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import cl.karibu.commons.web.json.serializers.JsonDateSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Boleta implements Serializable {

	private static final long serialVersionUID = -5083126344823921050L;
	
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date fechaEmision;
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date fechaVencimiento;

	private Integer paginas;
	private BigDecimal montoDocumento;

	private boolean legada;

	private String tpDocumento;
	private String nuFolio;

	private String identificadorIntelidata;

	private String urlParcialDescarga;

	private String urlParcialImagenes;
	private String urlParcialPrimeraImagen;

	private String urlParcialEnvio;

	private String sTipo;
	private String urlImagenesVisor;
	private String urlDescargarImagenesVisor;

	public String getUrlImagenesVisor() {
		return urlImagenesVisor;
	}

	public void setUrlImagenesVisor(String urlImagenesVisor) {
		this.urlImagenesVisor = urlImagenesVisor;
	}

	public String getUrlDescargarImagenesVisor() {
		return urlDescargarImagenesVisor;
	}

	public void setUrlDescargarImagenesVisor(String urlDescargarImagenesVisor) {
		this.urlDescargarImagenesVisor = urlDescargarImagenesVisor;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Integer getPaginas() {
		return paginas;
	}

	public void setPaginas(Integer paginas) {
		this.paginas = paginas;
	}

	public String getIdentificadorIntelidata() {
		return identificadorIntelidata;
	}

	public void setIdentificadorIntelidata(String identificadorIntelidata) {
		this.identificadorIntelidata = identificadorIntelidata;
	}

	public BigDecimal getMontoDocumento() {
		return montoDocumento;
	}

	public void setMontoDocumento(BigDecimal montoDocumento) {
		this.montoDocumento = montoDocumento;
	}

	public String getUrlParcialDescarga() {
		return urlParcialDescarga;
	}

	public void setUrlParcialDescarga(String urlDescarga) {
		this.urlParcialDescarga = urlDescarga;
	}

	public String getTpDocumento() {
		return tpDocumento;
	}

	public void setTpDocumento(String tpDocumento) {
		this.tpDocumento = tpDocumento;
	}

	public String getNuFolio() {
		return nuFolio;
	}

	public void setNuFolio(String nuFolio) {
		this.nuFolio = nuFolio;
	}

	public String getUrlParcialImagenes() {
		return urlParcialImagenes;
	}

	public void setUrlParcialImagenes(String urlParcialImagenes) {
		this.urlParcialImagenes = urlParcialImagenes;
	}

	public boolean isLegada() {
		return legada;
	}

	public void setLegada(boolean legada) {
		this.legada = legada;
	}

	public String getUrlParcialEnvio() {
		return urlParcialEnvio;
	}

	public void setUrlParcialEnvio(String urlParcialEnvio) {
		this.urlParcialEnvio = urlParcialEnvio;
	}

	public String getUrlParcialPrimeraImagen() {
		return urlParcialPrimeraImagen;
	}

	public void setUrlParcialPrimeraImagen(String urlParcialPrimeraImagen) {
		this.urlParcialPrimeraImagen = urlParcialPrimeraImagen;
	}

	public String getsTipo() {
		return sTipo;
	}

	public void setsTipo(String sTipo) {
		this.sTipo = sTipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaEmision == null) ? 0 : fechaEmision.hashCode());
		result = prime * result + ((fechaVencimiento == null) ? 0 : fechaVencimiento.hashCode());
		result = prime * result
				+ ((identificadorIntelidata == null) ? 0 : identificadorIntelidata.hashCode());
		result = prime * result + (legada ? 1231 : 1237);
		result = prime * result + ((montoDocumento == null) ? 0 : montoDocumento.hashCode());
		result = prime * result + ((nuFolio == null) ? 0 : nuFolio.hashCode());
		result = prime * result + ((paginas == null) ? 0 : paginas.hashCode());
		result = prime * result + ((sTipo == null) ? 0 : sTipo.hashCode());
		result = prime * result + ((tpDocumento == null) ? 0 : tpDocumento.hashCode());
		result = prime * result
				+ ((urlParcialDescarga == null) ? 0 : urlParcialDescarga.hashCode());
		result = prime * result + ((urlParcialEnvio == null) ? 0 : urlParcialEnvio.hashCode());
		result = prime * result
				+ ((urlParcialImagenes == null) ? 0 : urlParcialImagenes.hashCode());
		result = prime * result
				+ ((urlParcialPrimeraImagen == null) ? 0 : urlParcialPrimeraImagen.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boleta other = (Boleta) obj;
		if (fechaEmision == null) {
			if (other.fechaEmision != null)
				return false;
		} else if (!fechaEmision.equals(other.fechaEmision))
			return false;
		if (fechaVencimiento == null) {
			if (other.fechaVencimiento != null)
				return false;
		} else if (!fechaVencimiento.equals(other.fechaVencimiento))
			return false;
		if (identificadorIntelidata == null) {
			if (other.identificadorIntelidata != null)
				return false;
		} else if (!identificadorIntelidata.equals(other.identificadorIntelidata))
			return false;
		if (legada != other.legada)
			return false;
		if (montoDocumento == null) {
			if (other.montoDocumento != null)
				return false;
		} else if (!montoDocumento.equals(other.montoDocumento))
			return false;
		if (nuFolio == null) {
			if (other.nuFolio != null)
				return false;
		} else if (!nuFolio.equals(other.nuFolio))
			return false;
		if (paginas == null) {
			if (other.paginas != null)
				return false;
		} else if (!paginas.equals(other.paginas))
			return false;
		if (sTipo == null) {
			if (other.sTipo != null)
				return false;
		} else if (!sTipo.equals(other.sTipo))
			return false;
		if (tpDocumento == null) {
			if (other.tpDocumento != null)
				return false;
		} else if (!tpDocumento.equals(other.tpDocumento))
			return false;
		if (urlParcialDescarga == null) {
			if (other.urlParcialDescarga != null)
				return false;
		} else if (!urlParcialDescarga.equals(other.urlParcialDescarga))
			return false;
		if (urlParcialEnvio == null) {
			if (other.urlParcialEnvio != null)
				return false;
		} else if (!urlParcialEnvio.equals(other.urlParcialEnvio))
			return false;
		if (urlParcialImagenes == null) {
			if (other.urlParcialImagenes != null)
				return false;
		} else if (!urlParcialImagenes.equals(other.urlParcialImagenes))
			return false;
		if (urlParcialPrimeraImagen == null) {
			if (other.urlParcialPrimeraImagen != null)
				return false;
		} else if (!urlParcialPrimeraImagen.equals(other.urlParcialPrimeraImagen))
			return false;
		return true;
	}
}
