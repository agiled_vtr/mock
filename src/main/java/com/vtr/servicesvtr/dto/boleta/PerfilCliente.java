package com.vtr.servicesvtr.dto.boleta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PerfilCliente implements Serializable 
{
	private static final long serialVersionUID = 1L;
	
	private BigDecimal saldo;
	
	private BigDecimal deudaTotal;
	
	private boolean flagCobranza;
	private boolean flagSoloPrepago;
	private String rut;
	private String nombreCliente;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private boolean flagActivarEmail;
	private String emailContacto;
	private List<Cuenta> cuentas = new ArrayList<Cuenta>();
	
	public String getEmailContacto() 
	{
		return emailContacto;
	}

	public void setEmailContacto(String emailContacto) 
	{
		this.emailContacto = emailContacto;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public List<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	public BigDecimal getDeudaTotal() {
		return deudaTotal;
	}

	public void setDeudaTotal(BigDecimal deudaTotal) {
		this.deudaTotal = deudaTotal;
	}

	public boolean isFlagCobranza() {
		return flagCobranza;
	}

	public void setFlagCobranza(boolean flagCobranza) {
		this.flagCobranza = flagCobranza;
	}

	public boolean isFlagSoloPrepago() {
		return flagSoloPrepago;
	}

	public void setFlagSoloPrepago(boolean flagSoloPrepago) {
		this.flagSoloPrepago = flagSoloPrepago;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public boolean isFlagActivarEmail() {
		return flagActivarEmail;
	}

	public void setFlagActivarEmail(boolean flagActivarEmail) {
		this.flagActivarEmail = flagActivarEmail;
	}

	

}
