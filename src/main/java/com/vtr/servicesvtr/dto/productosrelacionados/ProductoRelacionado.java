/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vtr.servicesvtr.dto.productosrelacionados;

import java.io.Serializable;

/**
 *
 * @author druiz
 */

public class ProductoRelacionado implements Serializable{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String idServicio;
    public String mac;
  //Petición de Haibu
    public String serialNumber;
    
    public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
    
    
    
    
}
