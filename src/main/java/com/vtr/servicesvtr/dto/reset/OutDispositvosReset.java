/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vtr.servicesvtr.dto.reset;

import com.vtr.servicesvtr.dto.productosrelacionados.ProductoRelacionado;
import java.util.List;

/**
 *
 * @author druiz
 */
public class OutDispositvosReset 
{
     public String idServicio;
     public String tipoServicio;
     public String idVivienda;

     public List<ProductoRelacionado> productosRelacionados;

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    public List<ProductoRelacionado> getProductosRelacionados() {
        return productosRelacionados;
    }

    public void setProductosRelacionados(List<ProductoRelacionado> ProductosRelacionados) {
        this.productosRelacionados = ProductosRelacionados;
    }
     
     
    
}
